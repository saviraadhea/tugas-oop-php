<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Nama hewan: $sheep->name <br>"; // "shaun"
echo "Jumlah kaki: $sheep->legs <br>"; // 2
echo "Berdarah dingin: $sheep->cold_blooded <br><br>"; // false

$frog = new frog("buduk");
echo "Nama hewan: $frog->name <br>"; // "buduk"
echo "Jumlah kaki: $frog->legs <br>"; // 4
echo "Berdarah dingin: $frog->cold_blooded <br>"; // false
echo $frog->jump() . "<br><br>"; // "hop hop"

$ape = new ape("kera sakti");
echo "Nama hewan: $ape->name <br>"; // "kera sakti"
echo "Jumlah kaki: $ape->legs <br>"; // 4
echo "Berdarah dingin: $ape->cold_blooded <br>"; // false
echo $ape->yell(); // "Auooo"


?>